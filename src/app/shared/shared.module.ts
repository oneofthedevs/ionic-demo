import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IsRequiredDirective } from './isRequired/is-required.directive';
import { MinLengthDirective } from './minLength/min-length.directive';
import { MaxLengthDirective } from './maxLength/max-length.directive';
import { TextboxComponent } from './components/textbox/textbox.component';
import { RadioComponent } from './components/radio/radio.component';
import { ValidationMessageComponent } from './components/validation-message/validation-message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    IsRequiredDirective,
    MinLengthDirective,
    MaxLengthDirective,
    TextboxComponent,
    RadioComponent,
    ValidationMessageComponent,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    IsRequiredDirective,
    MaxLengthDirective,
    MinLengthDirective,
    TextboxComponent,
    RadioComponent,
  ],
})
export class SharedModule {}
