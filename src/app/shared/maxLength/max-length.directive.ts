import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { IsRequiredDirective } from '../isRequired/is-required.directive';

@Directive({
  selector: '[appMaxLength]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: IsRequiredDirective,
      multi: true,
    },
  ],
})
export class MaxLengthDirective implements Validator {
  constructor(private maxLengthValue: number) {}

  validate(control: AbstractControl): ValidationErrors {
    let response: ValidationErrors;

    if (control.value.length > this.maxLengthValue) {
      response = {
        message: `Cannot exceed ${this.maxLengthValue} characters`,
        isMinLength: false,
      };
    }

    return response;
  }
}
