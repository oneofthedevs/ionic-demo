import { Component, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroup,
} from '@angular/forms';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss'],
})
export class TextboxComponent<T> implements ControlValueAccessor {
  @Input() public label: string;
  @Input() public description: string | null;
  // @Input() public title: string | null;
  @Input() public isDisabled: boolean;
  @Input() public inputType: string;
  @Input() public value: T;
  // @Input() public isTouched: boolean;
  // @Input() public isChanged: (value: any) => void;
  // @Input() public parentForm: FormGroup;

  constructor() {}

  // public get control(): AbstractControl {
  //   return this.parentForm.get(this.label);
  // }

  public writeValue(value: T): void {
    if (value) {
      this.value = value;
    }
  }

  public registerOnChange(onChange: (value: any) => void): void {
    // this.isChanged = onChange;
  }

  public registerOnTouched(fn: boolean): void {
    // this.isTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }

  public getValidation(control: AbstractControl): boolean {
    return control.valid;
  }
}
