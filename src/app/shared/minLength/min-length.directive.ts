import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { IsRequiredDirective } from '../isRequired/is-required.directive';

@Directive({
  selector: '[appMinLength]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: IsRequiredDirective,
      multi: true,
    },
  ],
})
export class MinLengthDirective implements Validator {
  constructor(private minLengthValue: number) {}

  validate(control: AbstractControl): ValidationErrors {
    let response: ValidationErrors;

    if (control.value.length < this.minLengthValue) {
      response = {
        message: `Must be atleast ${this.minLengthValue} characters`,
        isMinLength: false,
      };
    }

    return response;
  }
}
