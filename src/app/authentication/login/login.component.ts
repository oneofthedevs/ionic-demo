import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public name = 'Name';
  public value = 'Hey there';
  public description = 'Moo';
  public isDisabled = false;
  constructor() {}

  ngOnInit() {}
}
