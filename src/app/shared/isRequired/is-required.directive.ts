import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Directive({
  selector: '[appIsRequired]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: IsRequiredDirective,
      multi: true,
    },
  ],
})
export class IsRequiredDirective implements Validator {
  constructor() {}

  public validate(control: AbstractControl): ValidationErrors {
    let response: ValidationErrors;

    if (control.value === null) {
      response = {
        required: 'This field is required',
        isRequired: false,
      };
    }
    return response;
  }
}
